# Description
Tool for generating AWS DynamoDB layout based on a YAML specification file.

It creates the tables specified using available AWS credentials in environment. If a table exists it asks for user input (default to skip the table).

There is no migration possible currently, so if you let it delete the table it will destroy all data in it as well!!!

# Issues

Please report all issues using "Issues" in BitBucket repository toolbar, thanks :)


# Installation

```
python setup.py install
```

or using pip:

```
pip install -e git+https://bitbucket.org/mbek/dynamo-generator.git#egg=dynamo-generator
```

# Usage

```
dynamo-gen.py specification.yml
```


# YAML specification file
## Format

```
TABLE_NAME:
    hash_key: F,some_key            # required, F is an AWS format string (S, SS, N...)

    range: true                     # if HashRange table is being created. Default 'false'
    range_key: F,key                # only needed if 'range: true'

    indexes: ['F,index1', 'F,index2']   # optional, list of Secondary Indexes
```

Indexes currently support only "ALL" for attribute projection (automatically).


## Example of YAML specification

```
websites:
    hash_key:  S,id

users:
    range: true
    hash_key: S,id
    range_key: S,uid
    indexes: ['S,subscription']
```
