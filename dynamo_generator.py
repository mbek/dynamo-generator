import os.path
import time

import boto.dynamodb                                                                                                   
from boto.exception import JSONResponseError
from boto.dynamodb2.exceptions import ResourceNotFoundException
from boto.dynamodb2.table import Table
from yaml import load
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class BotoGen:
    def __init__(self, region):
        self.conn = boto.dynamodb2.connect_to_region(region)
        
    def create_table(self, table, hash_key, range_key, indexes):
        print '\t...Creating table'
        table_exists = self.table_exists(table)

        if table_exists:
            inp = raw_input('\t...Table exists! Do you want to delete and recreate it? Y/[N] >>> ')
            overwrite = not(not inp) or inp == 'Y' or inp == 'y'
            if not overwrite:
                print '\t...Skipping!'
                return
            print '\t...Waiting for table %s to get deleted...' % table
            self.conn.delete_table(table)

            while self.table_exists(table):
                time.sleep(1)

        attrs = [{'AttributeName': hash_key.split(',')[1], 'AttributeType': hash_key.split(',')[0]}]
        schema = [{'AttributeName': hash_key.split(',')[1], 'KeyType': 'HASH'}]

        sec_ind = None
        if indexes:
            sec_ind = []
            for _index in indexes:
                index_type, index = _index.split(',')
                sec_ind = sec_ind + [{'IndexName': '%s-index' % index,
                                      'KeySchema': schema + [{'AttributeName': index, 'KeyType': 'RANGE'}],
                                      'Projection': {'ProjectionType': 'ALL', 'NonKeyAttributes': None}
                                    }]
                attrs = attrs + [{'AttributeName': index, 'AttributeType': index_type}]

        if range_key:
            attrs = attrs + [{'AttributeName': range_key.split(',')[1], 'AttributeType': range_key.split(',')[0]}]
            schema = schema + [{'AttributeName': range_key.split(',')[1], 'KeyType': 'RANGE'}]

        ret = self.conn.create_table(attribute_definitions=attrs,
                                     table_name=table,
                                     key_schema=schema,
                                     provisioned_throughput={'ReadCapacityUnits': 1, 'WriteCapacityUnits': 1},
                                     local_secondary_indexes=sec_ind
                                     )
        if ret:
            print '\tWaiting for table to get created...'
            while self.table_exists(table) != 'ACTIVE':
                time.sleep(1)
            print '\t...Created!'

    def table_exists(self, table):
        try:
            ret = self.conn.describe_table(table)
            return ret['Table']['TableStatus']
        except JSONResponseError as e:
            if 'ResourceNotFoundException' in e.body['__type']:
                return None
            raise

def parse(args):
    
    filename = args.input_file
    gen = BotoGen(args.region)

    if not os.path.exists(filename):
        raise IOError('Specified YAML file doesn\'t exist: %s' % filename)

    with open(filename, 'r') as f:
        data = load(f, Loader=Loader)

    for table_name in data.keys():
        table = data[table_name]
        print '\nTable: %s' % table_name
        if 'hash_key' not in table:
            print '\t*** Missing type or hash_key parameter!'
            continue
        range_table = 'range' in table and table['range'] or False
        if range_table and 'range_key' not in table:
            print '\t*** Missing range_key parameter!'
            continue
        print '\tHash: %s' % table['hash_key']
        if range_table:
            print '\tRange: %s' % table['range_key']

        if 'indexes' in table and len(table['indexes']) > 0:
            print '\tSecondary indexes:', table['indexes']

        gen.create_table(table_name,
                         table['hash_key'],
                         table['range_key'] if range_table else None,
                         table['indexes'] if 'indexes' in table else None)
