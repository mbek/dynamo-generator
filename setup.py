from setuptools import setup

setup(name='dynamo-generator',
      version='1.0',
      author='Marin Bek',
      author_email='marin.bek@gmail.com',
      description='Manage DynamoDB tables using YAML scripts',
      keywords='dynamodb boto yaml',
      url='https://bitbucket.org/mbek/dynamo-generator',
      py_modules=['dynamo_generator'],
      scripts=['bin/dynamo-gen.py'],
      install_requires=['boto>=2.9.6',
                        'argparse>=1.2.1',
                        'pyaml==13.05.2'
                       ],
      )
