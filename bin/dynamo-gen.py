import argparse

import dynamo_generator


parser = argparse.ArgumentParser(description='Generate AWS DynamoDB layout based on a YAML specification')
parser.add_argument('input_file', type=str,
                    help='YAML file with DB specification')

parser.add_argument('-r, --region', dest='region', action='store',
                    default='us-west-1',
                    help='Specify AWS region')

args = parser.parse_args()

dynamo_generator.parse(args)
